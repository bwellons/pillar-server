let ProductModel = require('../models/product'),
    Utils = require('../services/utils'),
    _ = require('lodash');

var fs = require('fs'),
    S3FS = require('s3fs'),
    s3fsImpl = new S3FS(process.env.S3_BUCKET_NAME, {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    });

let product = {};

product.create = () => {
    return (req, res) => {
        if (req.body.name) {
            let d = new Date();
            let prod = new ProductModel(_.extend({}, req.body, {
                lastUpdated: d.getTime(),
                organization: req.params.orgId
            }));

            prod.save((err, doc) => {
                if (err) {
                    res.status(500).send('Error saving product');
                } else {
                    res.send(doc);
                }
            })
        } else {
            res.status(400).send('Missing required key: name');
        }
    };
};

product.update = () => {
    return (req, res) => {
        let productId = req.params.productId;
        ProductModel.findOne({
            _id: productId
        }, function (err, doc) {
            if (err) {
                res.status(500).send('Error finding Doc');
            } else if (!doc._id) {
                res.status(400).send('No matching doc found');
            } else {
                doc = _.extend(doc, req.body, {
                    lastUpdated: Utils.getTimeStamp()
                });

                doc.save(function (err, updatedDoc) {
                    if (err) {
                        res.status(500).send('Error saving document');
                    } else {
                        res.send(updatedDoc);
                    }
                });
            }
        })
    };
};

product.remove = () => {
    return (req, res) => {
        let productId = req.params.productId;
        ProductModel.findOneAndRemove({
            _id: productId
        }, function (err, removedDoc, rawResult) {
            if (err) {
                res.status(500).send('Error removing doc from DB');
            } else {
                res.send({});
            }
        });
    };
};

product.getOne = () => {
    return (req, res) => {
        ProductModel.findOne({
            _id: req.params.prodId
        }, (err, doc) => {
            if (err) {
                res.status(500).send('Error finding product doc');
            } else {
                res.send(doc);
            }
        });
    };
};

product.getAll = () => {
    return (req, res) => {
        ProductModel.find({
            organization: req.params.orgId
        }, (err, docs) => {
            if (err) {
                res.status(500).send('Error finding product docs');
            } else {
                res.send(docs);
            }
        });
    };
};

product.upload = () => {
    return (req, res) => {
        if (!req.files) {
            res.status(400).send('Files array should be on request');
        } else {
            var file = req.files.file;
            var stream = fs.createReadStream(file.path);
            return s3fsImpl.writeFile(req.params.orgId + '/' + req.params.prodId + '/' + file.originalFilename, stream).then(function () {
                ProductModel.findOne({
                    _id: req.params.prodId
                }, function (err, doc) {
                    if (doc) {
                        if (doc.assets.indexOf(file.originalFilename) === -1) {
                            doc.assets.push(file.originalFilename);
                        }
                    }
                    doc.save((err, updatedDoc) => {
                        if (err) {
                            res.status(500).send('Error saving doc: ' + err);
                        } else {
                            fs.unlink(file.path, function (err) {
                                if (err) {
                                    console.error('Error: ' + err);
                                }
                            });
                            res.send(updatedDoc);
                        }
                    });
                });
            });
        }
    };
}

module.exports = product;
