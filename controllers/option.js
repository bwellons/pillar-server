let OptionModel = require('../models/option'),
	_ = require('lodash'),
	Utils = require('../services/utils');


let Option = {};


Option.create = () => {
	return (req, res) => {
		if (!req.body.name) {
			res.status(400).send('You must include: name');
		} else {
			let option = new OptionModel(_.extend({}, req.body, {
				lastUpdated: Utils.getTimeStamp(),
				productId: req.params.productId
			}));

			option.save((err, doc) => {
				if (err) {
					res.status(500).send('Error creating document: ' + err);
				} else {
					res.send(doc);
				}
			});
		}
	};
};

Option.getOption = () => {
	return (req, res) => {
		let optionId = req.params.optionId;
		OptionModel.findOne({
			_id: optionId
		}, (err, doc) => {
			if (err) {
				res.status(500).send('Error finding option');
			} else {
				res.send(doc);
			}
		});
	};
};

Option.updateOption = () => {
	return (req, res) => {
		let optionId = req.params.optionId;
		OptionModel.findOne({
			_id: optionId
		}, (err, doc) => {
			if (err) {
				res.status(500).send('Error finding Option to update');
			} else {
				doc = _.extend(doc, req.body, {
					lastUpdate: Utils.getTimeStamp()
				});

				doc.save((err, doc) => {
					if (err) {
						res.status(500).send('Error updating option');
					} else {
						res.send(doc);
					}
				});
			}
		});
	};
};

Option.deleteOption = () => {
	return (req, res) => {
		let optionId = req.params.optionId;
		OptionModel.findOneAndRemove({
			_id: optionId
		}, (err, removed, raw) => {
			if (err) {
				res.status(500).send('Error removing document');
			} else {
				res.send({});
			}
		});
	};
};

Option.getOptionsByProductId = () => {
	return (req, res) => {
		let productId = req.params.productId;
		OptionModel.find({
			productId: productId
		}, (err, docs) => {
			if (err) {
				res.status(500).send('Error finding docs');
			} else {
				res.send(docs);
			}
		});
	};
};



module.exports = Option;
