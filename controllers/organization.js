let OrgModel = require('../models/organization'),
	_ = require('lodash');


let organization = {};


organization.create = () => {
	return (req, res) => {
		if (req.body.name) {
			let updated = new Date();
			let org = new OrgModel(_.extend(req.body, { 
				lastUpdated: updated.getTime()
			}));

			org.save((err, doc) => {
				if (err) {
					res.status(500).send('Error saving organization')
				} else {
					res.send(doc);
				}
			});
		} else {
			res.status(500).send('Missing required fields: name');
		}
	};
};

organization.update = () => {
	return (req, res) => {
		if (req.body.name) { // TODO: What are the required fields?
			// TODO: Should verify the orgId is valid before continuing
			OrgModel.findOne({
				_id: req.params.orgId
			}, (err, doc) => {
				if (err) {
					res.status(500).send('Error finding organization to update');
				} else {
					let d = new Date();
					doc = _.extend(doc, req.body, {
						lastUpdated: d.getTime()
					});

					doc.save((err, updated, rowsAffected) => {
						if (err) {
							res.status(500).send('Error updating user document');
						} else {
							res.send(updated);
						}
					});
				}
			});
		} else {
			res.status(400).send('Missing required fields for updating');
		}
		
	};
}

organization.getOne = () => {
	return (req, res) => {
		OrgModel.findOne({
			_id: req.params.orgId
		}, (err, doc) => {
			if (err) {
				res.status(500).send('Error finding organization');
			} else {
				res.send(doc);
			}
		});
	};
};

// TODO: Should paginate results
organization.getAll = () => {
	return (req, res) => {
		OrgModel.find({}, (err, docs) => {
			if (err) {
				res.status(500).send('Error finding organization');
			} else {
				res.send(docs);
			}
		});
	};
};

organization.remove = () => {
	return (req, res) => {
		OrgModel.findOneAndRemove({
			_id: req.params.orgId
		}, (err, removed, raw) => {
			if (err) {
				res.status(500).send('Error removing org');
			} else {
				res.send({});
			}
		});
	}
};

module.exports = organization;