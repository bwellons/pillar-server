let UserModel = require('../models/user'),
	OrgModel = require('../models/organization'),
	uuid = require('uuid/v1'),
	_ = require('lodash');

let user = {};

user.create = () => {
	return (req, res) => {
		// verify org first
		OrgModel.findOne({
			_id: req.params.orgId
		}, (err, doc) => {
			if (err) {
				res.status(500).send('Error retrieving org doc');
			} else if (!doc._id) {
				res.status(400).send('Invalid organization');
			} else {
				if (req.body.username && req.body.password && req.body.email) {
					let d = new Date();
					let user = new UserModel(_.extend(req.body, { 
						organization: req.params.orgId,
						lastUpdated: d.getTime(),
						authToken: uuid()
					}));
					user.save((err, doc) => {
						if (err) {
							res.status(500).send('Error creating document');
							return;
						} 
						delete doc.password;
						res.send(doc);
					});
				} else {
					res.status(400).send('Missing one or more required keys: email, username, password');
				}
			}
		});
	};
};

user.login = () => {
	return (req, res) => {
		if (req.body.username && req.body.password) {
			UserModel.findOne({ 
				username: req.body.username,
				password: req.body.password
			}, (err, userDoc) => {
				if (err) {
					res.status(500).send('Error retrieving user');
				} else if (!userDoc._id) {
					res.status(403).send('Unauthorized Credentials');
				} else {
					let responseObject = {
						user: userDoc
					};
					delete responseObject.user.password;

					OrgModel.findOne({
						_id: userDoc.organization
					}, (err, orgDoc) => {
						if (err) {
							res.status(500).send('Error finding organization');
						} else {
							responseObject.organization = orgDoc;
							res.send(responseObject);			
						}
					});
				}
			});
		} else {
			res.status(403).send('Invalid login');
		}
	};
};

user.update = () => {
	return (req, res) => {
		delete req.body.organization; // Prevent someone from changing orgs
		
		UserModel.findOne({
			_id: req.params.userId
		}, (err, doc) => {
			if (err) {
				res.status(500).send('Error finding user to update');
			} else {
				var d = new Date();
				doc = _.extend(doc, req.body, {
					lastUpdated: d.getTime()
				});

				doc.save((err, updated, rowsAffected) => {
					if (err) {
						res.status(500).send('Error updating user document');
					} else {
						delete updated.password; // Don't transmit password
						res.send(updated);
					}
				});
			}
		});
	};
};

// TODO: Paginate responses
user.all = () => {
	return (req, res) => {
		UserModel.find({
			organization: req.params.orgId
		}, (err, docs) => {
			if (err) {
				res.status(500).send('Error getting users');
			} else {
				res.send(docs);
			}
		});
	};
};

user.get = () => {
	return (req, res) => {
		UserModel.findOne({
			_id: req.params.userId
		}, (err, doc) => {
			if (err) {
				res.status(500).send('Error getting user!');
			} else {
				// Don't send password back
				delete doc.password;

				res.send(doc);
			}
		});
	}
}

user.isAuthorized = function (authToken, callback) {
	UserModel.findOne({
		authToken: authToken
	}, function (err, doc) {
		if (err) {
			callback(null, false); // Not an authorized user
		} else if (doc._id) {
			callback(doc, true);
		} else {
			callback(null, false);
		}
	});
}

module.exports = user;