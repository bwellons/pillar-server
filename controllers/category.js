let CategoryModel = require('../models/category'),
	Utils = require('../services/utils'),
	_ = require('lodash');


let category = {};

category.create = () => {
	return (req, res) => {
		if (req.body.name) {
			let cat = new CategoryModel({
				name: req.body.name,
				organization: req.params.orgId,
				lastUpdated: Utils.getTimeStamp()
			});

			cat.save((err, doc) => {
				if (err) {
					res.status(500).send('Error Saving category');
				} else {
					res.send(doc);
				}
			});
		} else {
			res.status(400).send('Missing required keys: name')
		}
	};
};

category.remove = () => {
	return (req, res) => {
		CategoryModel.findOneAndRemove({
			_id: req.params.catId
		}, (err, doc, result) => {
			if (err) {
				res.status(500).send('Error finding ')
			} else {
				res.send();
			}
		});
	};
};

category.update = () => {
	return (req, res) => {
		if (req.body.name) {
			delete req.body.organization; // Don't switch orgs for categories

			CategoryModel.findOne({
				_id: req.params.catId
			}, (err, doc) => {
				if (err) {
					res.status(500).send('Error finding category');
				} else if (!doc._id) {
					res.status(400).send('Category does not exist');
				} else {
					doc.name = req.body.name;
					doc.lastUpdated = Utils.getTimeStamp()
					doc.save((err, doc, numAffected) => {
						if (err) {
							res.status(500).send('Error updating category');
						} else {
							res.send(doc);
						}
					});
				}
			});
		} else {
			res.status(400).send('Required fields missing: name')
		}
	};
};

category.all = () => {
	return (req, res) => {
		CategoryModel.find({
			organization: req.params.orgId
		}, function(err, docs) {
			if (err) {
				res.status(500).send('Error retrieving categories');
			} else {
				res.send(docs);
			}
		})
	};
}

module.exports = category;