let MediaModel = require('../models/media'),
    Utils = require('../services/utils'),
    _ = require('lodash');

var fs = require('fs'),
    S3FS = require('s3fs'),
    s3fsImpl = new S3FS(process.env.S3_BUCKET_NAME, {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    });

var Media = {};

Media.getAllByOrgId = () => {
    return (req, res) => {
        MediaModel.find({
            organization: req.params.orgId
        }, (err, docs) => {
            if (err) {
                res.status(500).send('Error retrieving media docs');
            } else {
                res.send(docs);
            }
        });
    };
};

Media.getOne = () => {
    return (req, res) => {
        MediaModel.findOne({
            _id: req.params.mediaId
        }, (err, doc) => {
            if (err) {
                res.status(500).send('Error finding media');
            } else {
                res.send(doc);
            }
        });
    };
};

Media.create = () => {
    return (req, res) => {
        if (!req.body.name) {
            res.status(400).send('Missing required key: name');
        } else {
            let newMedia = new MediaModel(_.extend({}, req.body, {
                lastUpdated: Utils.getTimeStamp(),
                organization: req.params.orgId
            }));

            newMedia.save((err, doc) => {
                if (err) {
                    res.status(500).send('Error creating doc: ' + err);
                } else {
                    res.send(doc);
                }
            });
        }
    };
};

Media.update = () => {
    return (req, res) => {
        if (req.body.name) {
            // Don't allow org to change
            delete req.body.organization;

            MediaModel.findOne({
                _id: req.params.mediaId
            }, (err, media) => {
                if (err) {
                    res.status(500).send('Error finding media doc');
                } else {
                    media = _.extend(media, req.body, {
                        lastUpdated: Utils.getTimeStamp()
                    });

                    media.save((err, mediaDoc) => {
                        if (err) {
                            res.status(500).send('Error updating doc: ' + err);
                        } else {
                            res.send(mediaDoc);
                        }
                    });
                }
            });

        } else {
            res.status(400).send('Missing required key: name');
        }
    };
};

Media.remove = () => {
    return (req, res) => {
        MediaModel.findOneAndRemove({
            _id: req.params.mediaId
        }, (err, rowsAffected, raw) => {
            if (err) {
                res.status(500).send('Error removing doc');
            } else {
                res.send({});
            }
        })
    }
}

Media.upload = () => {
    return (req, res) => {
        if (!req.files) {
            res.status(400).send('Files array should be on request');
        } else {
            var file = req.files.file;
            var stream = fs.createReadStream(file.path);
            return s3fsImpl.writeFile(req.params.orgId + '/' + req.params.mediaId + '/' + file.originalFilename, stream).then(function () {
                MediaModel.findOne({
                    _id: req.params.mediaId
                }, function (err, doc) {
                    if (doc) {
                        doc.original = file.originalFilename;
                    }
                    doc.save((err, updatedDoc) => {
                        if (err) {
                            res.status(500).send('Error saving doc' + err);
                        } else {
                            fs.unlink(file.path, function (err) {
                                if (err) {
                                    console.error('Error: ' + err);
                                }
                            });
                            res.send(updatedDoc);
                        }

                    });
                });
            });
        }

    };
};


module.exports = Media;
