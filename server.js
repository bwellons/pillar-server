let express = require('express'),
    app = express(),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty(),
    User = require('./controllers/user'),
    Organization = require('./controllers/organization'),
    Category = require('./controllers/category'),
    Product = require('./controllers/product'),
    Media = require('./controllers/media'),
    Option = require('./controllers/option');


app.use(cors());
app.use(bodyParser.json());

app.all('*', (req, res, next) => {
    if (req.path.indexOf('login') != -1) { // Logging in won't have header set yet
        next();
    } else {
        User.isAuthorized(req.get('Authorization'), function (user, isAuthenticated) {
            if (user && isAuthenticated) {
                next();
            } else {
                res.status(403).send('User not authorized');
            }
        });
    }

});

app.get('/api/status', (req, res) => {
    res.send('Pillar Server is running smoothly!');
});

// Organization routes
app.post('/api/org/create', Organization.create());
app.put('/api/org/update/:orgId', Organization.update());
app.get('/api/org/get/:orgId', Organization.getOne());
app.get('/api/org/getAll', Organization.getAll());
app.delete('/api/org/remove/:orgId', Organization.remove());


// User Routes
app.post('/api/user/create/:orgId', User.create());
app.post('/api/user/login', User.login());
app.put('/api/user/update/:userId', User.update());
app.get('/api/user/all/:orgId', User.all()); // For one orgId
app.get('/api/user/get/:userId', User.get());

// Category Routes
app.post('/api/category/create/:orgId', Category.create());
app.delete('/api/category/remove/:catId', Category.remove());
app.put('/api/category/update/:catId', Category.update());
app.get('/api/category/all/:orgId', Category.all());

// Product Routes
app.post('/api/product/create/:orgId', Product.create());
app.put('/api/product/update/:productId', Product.update());
app.delete('/api/product/remove/:productId', Product.remove());
app.get('/api/product/getOne/:prodId', Product.getOne());
app.get('/api/product/getAll/:orgId', Product.getAll());
app.post('/api/product/upload/:orgId/:prodId', multipartyMiddleware, Product.upload());

// Option Routes
app.post('/api/option/create/:productId', Option.create());
app.get('/api/option/getOption/:optionId', Option.getOption());
app.put('/api/option/update/:optionId', Option.updateOption());
app.delete('/api/option/remove/:optionId', Option.deleteOption());
app.get('/api/option/byProductId/:productId', Option.getOptionsByProductId());

// Media Routes
app.get('/api/media/getAll/:orgId', Media.getAllByOrgId());
app.get('/api/media/getOne/:mediaId', Media.getOne());
app.post('/api/media/create/:orgId', Media.create());
app.put('/api/media/update/:mediaId', Media.update());
app.delete('/api/media/remove/:mediaId', Media.remove());
app.post('/api/media/upload/:orgId/:mediaId', multipartyMiddleware, Media.upload());



function init() {

    mongoose.connect(process.env.MONGOLAB_URI); // Set from heroku env configs

    app.listen(process.env.PORT, (err) => {
        if (!err) {
            console.log(`Listening on port: ${process.env.PORT}`);
        }
    });

}


init();
