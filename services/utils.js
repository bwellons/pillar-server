let _ = require('lodash');

let utils = {};

utils.getTimeStamp = () => {
	let stamp = new Date();
	return stamp.getTime();
}

module.exports = utils;