let mongoose = require('mongoose'),
	Schema = mongoose.Schema;


let OrgSchema = new Schema({
	name: {
		type: String
	},
	address: {
		type: String
	},
	city: {
		type: String
	},
	state: {
		type: String
	},
	zip: {
		type: String
	},
	website: {
		type: String
	},
	salesEmail: {
		type: String
	},
	supportEmail: {
		type: String
	},
	salesPhone: {
		type: String
	},
	supportPhone: {
		type: String
	},
	logoImage: {
		type: String
	},
	enabled: {
		type: Boolean
	},
	lastUpdated: {
		type: Number
	}
});

module.exports = mongoose.model('Organization', OrgSchema);