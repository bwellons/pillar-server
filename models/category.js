let mongoose = require('mongoose'),
	Schema = mongoose.Schema;


let CategorySchema = new Schema({
	name: {
		type: String
	},
	organization: {
		type: String
	},
	lastUpdated: {
		type: Number
	}
});

module.exports = mongoose.model('Category', CategorySchema);