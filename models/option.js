let mongoose = require('mongoose'),
	Schema = mongoose.Schema;


let OptionSchema = new Schema({
	name: {
		type: String
	},
	description: {
		type: String
	},
	sku: {
		type: String
	},
	color: {
		type: String
	},
	price: {
		type: Number
	},
	enabled: {
		type: Boolean,
		default: true
	},
	displayOrder: {
		type: Number,
		default: 0
	},
	lastUpdated: {
		type: Number
	},
	productId: {
		type: String
	},
	category: {
		type: String
	}
});

module.exports = mongoose.model('Option', OptionSchema);