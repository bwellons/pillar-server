let mongoose = require('mongoose'),
	Schema = mongoose.Schema;


let UserSchema = new Schema({
	username: {
		type: String
	},
	name: {
		type: String
	},
	password: {
		type: String
	},
	address: {
		type: String
	},
	city: {
		type: String
	},
	state: {
		type: String
	},
	zip: {
		type: String
	},
	phoneMobile: {
		type: String
	},
	phoneHome: {
		type: String
	},
	phoneOffice: {
		type: String
	},
	email: {
		type: String
	},
	role: {
		type: String
	},
	organization: {
		type: String
	},
	enabled: {
		type: Boolean,
		default: true
	},
	authToken: {
		type: String
	},
	store: {
		type: String
	},
	lastUpdated: {
		type: Number
	}
});

module.exports = mongoose.model('User', UserSchema);