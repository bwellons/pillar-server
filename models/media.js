let mongoose = require('mongoose'),
	Schema = mongoose.Schema;


let MediaSchema = new Schema({
	name: {
		type: String
	},
	description: {
		type: String
	},
	thumbnail: {
		type: String
	},
	original: {
		type: String
	},
	enabled: {
		type: Boolean,
		default: true
	},
	displayOrder: {
		type: Number,
		default: 0
	},
	lastUpdated: {
		type: Number
	},
	category: {
		type: String
	},
	organization: {
		type: String
	}
});

module.exports = mongoose.model('Media', MediaSchema);