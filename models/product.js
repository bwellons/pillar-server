let mongoose = require('mongoose'),
	Schema = mongoose.Schema;


let ProductSchema = new Schema({
	name: {
		type: String
	},
	sku: {
		type: String
	},
	description: {
		type: String
	},
	category: {
		type: String
	},
	price: {
		type: Number
	},
	organization: {
		type: String
	},
	assets: [ String ],
	displayOrder: {
		type: Number,
		default: 0
	},
	enabled: {
		type: Boolean,
		default: true
	},
	lastUpdated: {
		type: Number
	},
	modelYear: {
		type: String
	},
	specs: [{
		name: String,
		value: String
	}]
});

module.exports = mongoose.model('Product', ProductSchema);